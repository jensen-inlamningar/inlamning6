public class ovningar {
    public static void main(String[] args) {

        //Skapar 3 objekt av klassen book
        Book book = new Book("The Hobbing", "J.R.R Toliken",
                "George Allen & Unwin","N02MNOP9032");
        Book book1 = new Book("Harry Potter", "J.K. Rowling"
                , "Blommsbury publisher", "03DFEY4543");
        Book book2 = new Book("Game of thrones", "George R.R. Martin",
                "Voyager Books", "N134LOP");


        //kallar på metoden displayBook
        book.displayBook();
        book1.displayBook();
        book2.displayBook();

    }
}
