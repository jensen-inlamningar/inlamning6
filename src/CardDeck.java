import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CardDeck {

    //Här skapar jag en list med kort med olika valörer och nr/klätt
    private List<Card> deck = new ArrayList<>(List.of(
            new Card("Diamond", "Ace"),
            new Card("Diamond", "2"),
            new Card("Diamond", "3"),
            new Card("Diamond", "4"),
            new Card("Diamond", "5"),
            new Card("Diamond", "6"),
            new Card("Diamond", "7"),
            new Card("Diamond", "8"),
            new Card("Diamond", "9"),
            new Card("Diamond", "10"),
            new Card("Diamond", "Jack"),
            new Card("Diamond", "Queen"),
            new Card("Diamond", "King"),
            new Card("Heart" , "Ace"),
            new Card("Heart" , "2"),
            new Card("Heart" , "3"),
            new Card("Heart" , "4"),
            new Card("Heart" , "5"),
            new Card("Heart" , "6"),
            new Card("Heart" , "7"),
            new Card("Heart" , "8"),
            new Card("Heart" , "9"),
            new Card("Heart" , "10"),
            new Card("Heart" , "Jack"),
            new Card("Heart" , "Queen"),
            new Card("Heart" , "King"),
            new Card("Spades", "Ace"),
            new Card("Spades", "2"),
            new Card("Spades", "3"),
            new Card("Spades", "4"),
            new Card("Spades", "5"),
            new Card("Spades", "6"),
            new Card("Spades", "7"),
            new Card("Spades", "8"),
            new Card("Spades", "10"),
            new Card("Spades", "Jack"),
            new Card("Spades", "Queen"),
            new Card("Spades", "King"),
            new Card("Clubs", "Ace"),
            new Card("Clubs", "2"),
            new Card("Clubs", "3"),
            new Card("Clubs", "4"),
            new Card("Clubs", "5"),
            new Card("Clubs", "6"),
            new Card("Clubs", "7"),
            new Card("Clubs", "8"),
            new Card("Clubs", "9"),
            new Card("Clubs", "10"),
            new Card("Clubs", "Jack"),
            new Card("Clubs", "Queen"),
            new Card("Clubs", "King")

    )

    );

    //Här shufflar jag korten
    public void shuffle(){
        Collections.shuffle(deck);
    }

    //printar ut objektet
    public void printFirstCard(){
        Card index = deck.get(0);
        System.out.println(index.getRank() + " of " + index.getSuit());
    }
}
