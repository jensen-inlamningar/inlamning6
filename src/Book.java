public class Book {
    private String title;
    private String author;
    private String publisher;
    private String ISBN;

    //Skapar en konstruktor
    public Book(String title, String author, String publisher, String ISBN) {
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.ISBN = ISBN;
    }

    //Printar ut objektet
    public void displayBook(){
        System.out.println(
                "Book:" + "\n" +
                        "Title: " + title + "\n" +
                        " Author: " + author + "\n" +
                        " Publisher: " + publisher + "\n" +
                        " ISBN: " + ISBN );
    }
}
