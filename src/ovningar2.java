public class ovningar2 {
    public static void main(String[] args) {

        //Skapar ett objekt av klassen CardDeck
        CardDeck cardDeck = new CardDeck();
        //Kallar på metoden shuffle
        cardDeck.shuffle();
        //Kallar på metoden print
        cardDeck.printFirstCard();

    }
}
